#!/bin/bash

# assumindo que a imagem do Ex1 se chama debian-with-vim:1.0
# se não, volta lá em manda um   $ docker build -t debian-with-vim:1.0 .

docker volume create myVol
docker run -d --rm --name webserver1 -v myVol:/usr/share/nginx/html -p 8080:80 nginx:latest
docker run -d --rm --name webserver2 -v myVol:/usr/share/nginx/html -p 8090:80 nginx:latest
docker run -ti --rm --name console -v myVol:/usr/share/nginx/html debian-with-vim:1.0

# no terminal do debian:
cd usr/share/nginx/html
echo "<p>Front Page</p>" > index.html
# checar localhost:8080 e localhost:8090 se aparece "Front Page"

# VIM por linha de comando num host Windows é muuuito bugado; fica um pouco melhor com PowerShell 7
# "na minha máquina funciona"